// Utilities.

#ifndef JERASURE_INCLUDED__INTERNAL_H
#define JERASURE_INCLUDED__INTERNAL_H

#include "config.h"

#ifdef HAVE_VAR_ATTRIBUTE_UNUSED
#ifdef HAVE_VAR_ATTRIBUTE_DEPRECATED
#define UNUSED(x) x __attribute__((__unused__)) __attribute__((__deprecated__))
#else
#define UNUSED(x) x __attribute__((__unused__))
#endif
#else
#define UNUSED(x) x
#endif
#endif

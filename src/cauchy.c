/* *
 * Copyright (c) 2014, James S. Plank and Kevin Greenan
 * All rights reserved.
 *
 * Jerasure - A C/C++ Library for a Variety of Reed-Solomon and RAID-6 Erasure
 * Coding Techniques
 *
 * Revision 2.0: Galois Field backend now links to GF-Complete
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 *  - Neither the name of the University of Tennessee nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* Jerasure's authors:

   Revision 2.x - 2014: James S. Plank and Kevin M. Greenan
   Revision 1.2 - 2008: James S. Plank, Scott Simmerman and Catherine D. Schuman.
   Revision 1.0 - 2007: James S. Plank
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "galois.h"
#include "jerasure.h"
#include "cauchy.h"

static int PPs[33] = { -1, -1, -1, -1, -1, -1, -1, -1,
                       -1, -1, -1, -1, -1, -1, -1, -1,
                       -1, -1, -1, -1, -1, -1, -1, -1,
                       -1, -1, -1, -1, -1, -1, -1, -1, -1 };
static int NOs[33];
static int ONEs[33][33];

static int *cbest_0;
static int *cbest_1;

static int cbest_2[3] = { 1, 2, 3 };

static int cbest_3[7] = { 1, 2, 5, 4, 7, 3, 6 };

static int cbest_4[15] = { 1, 2, 9, 4, 8, 13, 3, 6, 12, 5, 11, 15, 10, 14, 7 };

static int cbest_5[31] = { 1, 2, 18, 4, 9, 8, 22, 16, 3, 11, 19, 5, 10, 6, 20, 27, 13, 23, 26, 12,
    17, 25, 24, 31, 30, 7, 15, 21, 29, 14, 28 };

static int cbest_6[63] = { 1, 2, 33, 4, 8, 49, 16, 32, 57, 3, 6, 12, 24, 48, 5, 35, 9, 37, 10, 17,
    41, 51, 56, 61, 18, 28, 53, 14, 20, 34, 7, 13, 25, 36, 59, 26, 39, 40, 45, 50, 60, 52, 63,
    11, 30, 55, 19, 22, 29, 43, 58, 15, 21, 38, 44, 47, 62, 27, 54, 42, 31, 23, 46 };

static int cbest_7[127] = { 1, 2, 68, 4, 34, 8, 17, 16, 76, 32, 38, 3, 64, 69, 5, 19, 35, 70, 6, 9,
    18, 102, 10, 36, 85, 12, 21, 42, 51, 72, 77, 84, 20, 25, 33, 50, 78, 98, 24, 39, 49, 100, 110
   , 48, 65, 93, 40, 66, 71, 92, 7, 46, 55, 87, 96, 103, 106, 11, 23, 37, 54, 81, 86, 108, 13,
    22, 27, 43, 53, 73, 80, 14, 26, 52, 74, 79, 99, 119, 44, 95, 101, 104, 111, 118, 29, 59, 89,
    94, 117, 28, 41, 58, 67, 88, 115, 116, 47, 57, 83, 97, 107, 114, 127, 56, 82, 109, 113, 126,
    112, 125, 15, 63, 75, 123, 124, 31, 45, 62, 91, 105, 122, 30, 61, 90, 121, 60, 120 };

static int cbest_8[255] = {
#include "cauchy-best-8.inc"
};

static int cbest_9[511] = {
#include "cauchy-best-9.inc"
};

static int cbest_10[1023] = {
#include "cauchy-best-10.inc"
};

static int cbest_11[1023] = {
#include "cauchy-best-11.inc"
};

static int *cbest_12, *cbest_13, *cbest_14, *cbest_15, *cbest_16, *cbest_17, *cbest_18, *cbest_19, *cbest_20,
           *cbest_21, *cbest_22, *cbest_23, *cbest_24, *cbest_25, *cbest_26, *cbest_27, *cbest_28, *cbest_29, *cbest_30,
           *cbest_31, *cbest_32;

static int cbest_max_k[33] = { -1, -1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 1023, -1,
     -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
     -1, -1, -1, -1 };

static int cbest_init = 0;

static int *cbest_all[33];


#define talloc(type, num) (type *) malloc(sizeof(type)*(num))

int cauchy_n_ones(int n, int w)
{
  int no;
  int cno;
  int nones;
  int i, j;
  int highbit;

  highbit = (1 << (w-1));

  if (PPs[w] == -1) {
    nones = 0;
    PPs[w] = galois_single_multiply(highbit, 2, w);
    for (i = 0; i < w; i++) {
      if (PPs[w] & (1 << i)) {
        ONEs[w][nones] = (1 << i);
        nones++;
      }
    }
    NOs[w] = nones;
  }

  no = 0;
  for (i = 0; i < w; i++) if (n & (1 << i)) no++;
  cno = no;
  for (i = 1; i < w; i++) {
    if (n & highbit) {
      n ^= highbit;
      n <<= 1;
      n ^= PPs[w];
      cno--;
      for (j = 0; j < NOs[w]; j++) {
        cno += (n & ONEs[w][j]) ? 1 : -1;
      }
    } else {
      n <<= 1;
    } 
    no += cno;
  }
  return no;
}
  
int *cauchy_original_coding_matrix(int k, int m, int w)
{
  int *matrix;
  int i, j, index;

  if (w < 31 && (k+m) > (1 << w)) return NULL;
  matrix = talloc(int, k*m);
  if (matrix == NULL) return NULL;
  index = 0;
  for (i = 0; i < m; i++) {
    for (j = 0; j < k; j++) {
      matrix[index] = galois_single_divide(1, (i ^ (m+j)), w);
      index++;
    }
  }
  return matrix;
}

int *cauchy_xy_coding_matrix(int k, int m, int w, int *X, int *Y)
{
  int index, i, j;
  int *matrix;

  matrix = talloc(int, k*m);
  if (matrix == NULL) { return NULL; }
  index = 0;
  for (i = 0; i < m; i++) {
    for (j = 0; j < k; j++) {
      matrix[index] = galois_single_divide(1, (X[i] ^ Y[j]), w);
      index++;
    }
  }
  return matrix;
}

void cauchy_improve_coding_matrix(int k, int m, int w, int *matrix)
{
  int index, i, j, x;
  int tmp;
  int bno, tno, bno_index;

  for (j = 0; j < k; j++) {
    if (matrix[j] != 1) {
      tmp = galois_single_divide(1, matrix[j], w);
      index = j;
      for (i = 0; i < m; i++) {
        matrix[index] = galois_single_multiply(matrix[index], tmp, w);
        index += k;
      }
    }
  }
  for (i = 1; i < m; i++) {
    bno = 0;
    index = i*k;
    for (j = 0; j < k; j++) bno += cauchy_n_ones(matrix[index+j], w);
    bno_index = -1;
    for (j = 0; j < k; j++) {
      if (matrix[index+j] != 1) {
        tmp = galois_single_divide(1, matrix[index+j], w);
        tno = 0;
        for (x = 0; x < k; x++) {
          tno += cauchy_n_ones(galois_single_multiply(matrix[index+x], tmp, w), w);
        }
        if (tno < bno) {
          bno = tno;
          bno_index = j;
        }
      }
    }
    if (bno_index != -1) {
      tmp = galois_single_divide(1, matrix[index+bno_index], w);
      for (j = 0; j < k; j++) {
        matrix[index+j] = galois_single_multiply(matrix[index+j], tmp, w);
      }
    }
  }
}

int *cauchy_good_general_coding_matrix(int k, int m, int w)
{
  int *matrix, i;

  if (m == 2 && k <= cbest_max_k[w]) {
    matrix = talloc(int, k*m);
    if (matrix == NULL) return NULL;
    if (!cbest_init) {
      cbest_init = 1;
      cbest_all[0] = cbest_0; cbest_all[1] = cbest_1; cbest_all[2] = cbest_2; cbest_all[3] = cbest_3; cbest_all[4] =
      cbest_4; cbest_all[5] = cbest_5; cbest_all[6] = cbest_6; cbest_all[7] = cbest_7; cbest_all[8] = cbest_8;
      cbest_all[9] = cbest_9; cbest_all[10] = cbest_10; cbest_all[11] = cbest_11; cbest_all[12] = cbest_12;
      cbest_all[13] = cbest_13; cbest_all[14] = cbest_14; cbest_all[15] = cbest_15; cbest_all[16] = cbest_16;
      cbest_all[17] = cbest_17; cbest_all[18] = cbest_18; cbest_all[19] = cbest_19; cbest_all[20] = cbest_20;
      cbest_all[21] = cbest_21; cbest_all[22] = cbest_22; cbest_all[23] = cbest_23; cbest_all[24] = cbest_24;
      cbest_all[25] = cbest_25; cbest_all[26] = cbest_26; cbest_all[27] = cbest_27; cbest_all[28] = cbest_28;
      cbest_all[29] = cbest_29; cbest_all[30] = cbest_30; cbest_all[31] = cbest_31; cbest_all[32] = (int *) cbest_32;
    }
    for (i = 0; i < k; i++) {
      matrix[i] = 1;
      matrix[i+k] = cbest_all[w][i];
    }
    return matrix;
  } else {
    matrix = cauchy_original_coding_matrix(k, m, w);
    if (matrix == NULL) return NULL;
    cauchy_improve_coding_matrix(k, m, w, matrix);
    return matrix;
  }
}
